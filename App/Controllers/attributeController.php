<?php
/*
This file is part of OCAPI
Opencart HTTP(s) XML/JSON API

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues]

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Dge;

class attributeController extends \Dge\Controller {

  function __construct($params, $ct, $reg) {
    parent::__construct($ct, $reg);
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {
      case 'GET':
      case 'POST':
      case 'PUT':
        if($method === 'PUT'){
          $method = 'POST';
        }
        $task = 'index';
        if(count($params) === 2 && $params[0] === 'delete'){
          $method = strtoupper(array_shift($params));
        }
        break;
      case 'DELETE':
        $task = 'index';
        break;
      default:
        $task = '';
        break;
    }
    $this->handleTask($task, $params, $method);
  }

  private function handleTask($task, $params, $method = 'GET'){
    switch ($task.'_'.$method) {
      case 'index_GET':
        $this->load->model('catalog/attribute');
        if($method === 'GET'){
          $id = $this->getIdParam($params);
          if($id){
            $data = $this->model_catalog_attribute->getAttribute($id);
            if(!$data){
              \Dge\Error::write($this,'E404');
              die;
            }
            unset($data['language_id']);
            unset($data['name']);
            unset($data['description']);
            $data = array_merge($data,
              array('attribute_description' => $this->model_catalog_attribute->getAttributeDescriptions($id))
            );
            $data['attribute_description'] =$this->stranslateLocaleFromId(
              $data['attribute_description']
            );
            $this->writeHeaders();
            $this->write($data, 'attribute');
            die;
          }
          $filter = array();
          isset($_REQUEST['start']) ? $filter['start'] = (int)$_REQUEST['start'] : $filter['start'] = 0;
          isset($_REQUEST['limit']) ? $filter['limit'] = (int)$_REQUEST['limit'] : $filter['limit'] = 20;
          $data = $this->model_catalog_attribute->getAttributes($filter);
          $i = 0;
          $len = count($data);
          for($i = 0; $i < $len; $i++){
            if(array_key_exists('attribute_description', $data[$i])){
              $data[$i]['attribute_description'] =$this->stranslateLocaleFromId(
                $data[$i]['attribute_description']
              );
            }
          }
          if($len < 1){
              \Dge\Error::write($this,'E404');
          }else{
            $this->writeHeaders();
            $this->write($data, 'attribute');
          }
        }
        break;
      case 'index_POST':
      case 'index_PUT':
        $id = $this->getIdParam($params);
        $data = $this->getPostData();
        if(!$data){
          \Dge\Error::write($this,'E400', 'Posted data is not readable');
        }
        $this->upsert($id,$data);
        break;
      case 'index_DELETE':
        $id = $this->getIdParam($params);
        $this->delete($id);
        break;
      default:
        \Dge\Error::write($this,'E501', 'Not Implemented');
        break;
    }
  }


  private function upsert($id, $data){
    $status = '200 Ok';
    $new = false;
    list($field_error, $data) = $this->validateAndTransform($data);
    if($field_error){
      \Dge\Error::write($this,'E001',$field_error);
      die;
    }
    $this->load->model('catalog/attribute');
    if(!$id){
      // Set defaults
      if(!isset($data['sort_order'])){
        $data['sort_order'] = 0;
      }
      if(!isset($data['attribute_description'])){
        \Dge\Error::write($this,'E002','attribute_description');
        die;
      }
      if(!isset($data['attribute_group_id'])){
        \Dge\Error::write($this,'E002','attribute_group_id');
        die;
      }
      $pdesc = $this->stranslateLocaleFromId($data['attribute_description']);

      if(! isset($pdesc['nl']) || ! isset($pdesc['nl']['name'])){
        \Dge\Error::write($this,'E002','attribute_description - nl - name');
      }
      $data['attribute_description'] = $this->CopyDescriptions($pdesc['nl']);
      $id = $this->model_catalog_attribute->addAttribute($data);

      $result = array();
      $result['status'] = 'ok';
      $result['attribute_id'] = $id;
      $this->setResponseStatus('200 Ok');
      $this->writeHeaders();
      $this->write($result, 'attribute');
    }
    $old_data = $this->model_catalog_attribute->getAttribute($id);
    if(!$old_data){
      \Dge\Error::write($this,'E404');
      die;
    }
    unset($old_data['language_id']);
    unset($old_data['name']);
    $old_data = array_merge($old_data,
      array('attribute_description' => $this->model_catalog_attribute->getAttributeDescriptions($id))
    );
    $data = $this->mergeData($old_data, $data);
    $this->model_catalog_attribute->editAttribute($id, $data);
    $result = array();
    $result['status'] = 'ok';
    $result['attribute_id'] = $id;
    $this->setResponseStatus('200 Ok');
    $this->writeHeaders();
    $this->write($result, 'attribute');
    die;
  }

  private function delete($id){
    $this->load->model('catalog/attribute');
    $test = $this->model_catalog_attribute->getAttribute($id);
    if(!$test || $id < 1){
      \Dge\Error::write($this,'E404');
    }else{
      unset($test);
    }
    // OC model delete
    $this->model_catalog_attribute->deleteAttribute($id);
    $result['status'] = 'deleted';
    $result['attribute_id'] = $id;
    $this->setResponseStatus('200 Ok');
    $this->writeHeaders();
    $this->write($result, 'attribute');

  }

  // return TRUE on success
  protected function validate($type, $value){
    $res = false;
    switch ($type) {
      // Ints
      case 'attribute_group_id':
      case 'attribute_id':
      case 'sort_order':
      case 'language_id':
        $res = ( is_numeric($value)
                  && is_integer((int)$value)
                  && (int)$value.'' == $value
                );
        break;
      // tiny ints
      case 'approval':
        $res = (
          is_numeric($value)
          && is_integer((int)$value)
          && ( (int)$value == 0 || (int)$value == 1) );
        break;
      case 'name':
        $res = (strlen($value.'') > 2 && strlen($value.'') < 101);
        break;
      case 'description':
        $res = (is_string($value) || is_numeric($value));
        break;
      case 'created':
      case 'date_added':
      case 'date_modified':
      case 'date_start':
      case 'date_end':
        // yyyy-mm-dd
        $pattern = '/^\d{4}-\d{2}-\d{2}$/';
        $test = preg_match ( $pattern , $value );
        if($test === 1){
          $res = true;
        }else{
          $res = false;
        }
        break;
      default:

        break;
    }
    return $res;
  }

  protected function validateArray($field, $arr){
    $res = true;
    for($i = 0; $i < count($arr); $i++){
      $res = $this->validate($field, $arr[$i]);
      if(!$res){
        return false;
      }
    }
    return true;
  }

  protected function validateAndTransform($data){
    $res = true;
    // Cleanup data which we should NOT recieve / overwrite
    foreach ($data as $key => $value) {
      $field = $key;
      if($key === 'attribute_description'){
          list($err, $new_ar) = $this->stranslateLocaleFromISO2($value);
          if($err){
            return array( 'attribute_description : '.$err, false);
          }else{
            $data[$key] = $new_ar;
            $res = true;
          }
      }else if(is_array($value) && ! \Dge\App::is_assoc($value) ){
        $res = $this->validateArray($field, $value);
        if(!$res){
          return array($key, false);
        }
      }else{
          $res = $this->validate($field, $value);
      }
        if(!$res){
          return array($key, false);
        }
      }
      return array(false, $data);
  }
}
