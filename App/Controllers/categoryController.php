<?php
/*
This file is part of OCAPI
Opencart HTTP(s) XML/JSON API

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues]

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Dge;


class categoryController extends \Dge\Controller {

  function __construct($params, $ct, $reg) {
    parent::__construct($ct, $reg);
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {
      case 'GET':
      case 'POST':
      case 'PUT':
        if($method === 'PUT'){
          $method = 'POST';
        }
        $task = 'index';
        if(count($params) === 2 && $params[0] === 'delete'){
          $method = strtoupper(array_shift($params));
        }
        break;
      case 'DELETE':
        $task = 'index';
        break;
      default:
        $task = '';
        break;
    }
    $this->handleTask($task, $params, $method);
  }

  private function handleTask($task, $params, $method = 'GET'){

    switch ($task.'_'.$method) {
      case 'index_GET':
        $this->load->model('catalog/category');
        if($method === 'GET'){
          $category_id = $this->getIdParam($params);
          if($category_id){
            $this->get($category_id);
            return;
          }else{
            $this->index();
          }
        }
        break;
      case 'index_POST':
      case 'index_PUT':
        $this->load->model('catalog/category');
        $category_id = $this->getIdParam($params);
        $data = $this->getPostData();
        // We need POST data
        if(!$data){
          \Dge\Error::write($this,'E400', 'Posted data is not readable');
        }
        $this->upsert($category_id,$data);
        break;
      case 'index_DELETE':
        $category_id = $this->getIdParam($params);
        $this->load->model('catalog/category');
        // We need a dge_id as param
        $this->delete($category_id);

        break;
      default:
        \Dge\Error::write($this,'E501', 'Not Implemented');
        break;
    }
  }

  private function index(){
    $filter = array();
    isset($_REQUEST['start']) ? $filter['start'] = (int)$_REQUEST['start'] : $filter['start'] = 0;
    isset($_REQUEST['limit']) ? $filter['limit'] = (int)$_REQUEST['limit'] : $filter['limit'] = 20;
    $filter['sort'] = 'name';
    $data = $this->model_catalog_category->getCategories($filter);
    $i = 0;
    $len = count($data);
    if($len === 0){
      // Not found / no result
      $this->setResponseStatus('404 Not Found');
    }
    $dgeCatalogus = \Dge\App::loadModel('product');
    $data[$i]['reference'] = array();
    for($i = 0; $i < $len; $i++){
      $na = explode('&nbsp;&nbsp;&gt;&nbsp;&nbsp;',$data[$i]['name']);
      $data[$i]['name'] = $na[count($na)-1];
      $data[$i]['products'] = array();
      $data[$i]['products']['product_id'] = array();
      $prows = $this->getProductIdsByCategory($data[$i]['category_id']);
      $plen = count($prows);
      if($plen>0){
        for($i2=0;$i2<$plen;$i2++){

          $unit = array();
          // jan contains the DGE catalogus id
          // this can be used to reference opencart products
          // to dge catalogus id's when a dge_reference is missing
          $unit['catalogus_id'] = $dgeCatalogus->getJan($prows[$i2]['product_id']);
          $unit['product_id'] = $prows[$i2]['product_id'];
          $unit['dge_reference'] = $dgeCatalogus->getDgeReference($prows[$i2]['product_id']);

          $data[$i]['reference'][] = $unit;
        }
      }
    }
    $this->writeHeaders();
    $this->write($data, 'category');
  }

  private function get($category_id){
    $data = $this->model_catalog_category->getCategory($category_id);
    if(!$data){
      $this->setResponseStatus('404 Not Found');
      $this->writeHeaders();
      $this->write(array(), 'category');
      die;
    }
    unset($data['language_id']);
    unset($data['name']);
    unset($data['description']);
    unset($data['meta_description']);
    unset($data['meta_title']);
    unset($data['meta_keyword']);
    $data = array_merge($data,
      array('category_description' => $this->model_catalog_category->getCategoryDescriptions($category_id))
    );
    $data = array_merge($data,
      array('category_filter' => $this->model_catalog_category->getCategoryFilters($category_id))
    );
    $data = array_merge($data,
      array('category_store' => $this->model_catalog_category->getCategoryStores($category_id))
    );
    $data = array_merge($data,
      array('category_layout' => $this->model_catalog_category->getCategoryLayouts($category_id))
    );
    $data['products'] = $this->getProductIdsByCategory($category_id);

    $data['category_description'] =$this->stranslateLocaleFromId(
      $data['category_description']
    );
    $this->writeHeaders();
    $this->write($data, 'category');
    die;
  }

  # UPSERT
  # if $category_id == false
  # we do an INSERT
  # ELSE an update
  private function upsert($category_id, $data){
    $status = '200 Ok';
    $new = false;
    list($field_error, $data) = $this->validateAndTransform($data);
    if($field_error){
      \Dge\Error::write($this,'E001',$field_error);
    }
    if(!$category_id){
      // INSERT
      // Set defaults
      if(!isset($data['parent_id'])){
        $data['parent_id'] = 0;
      }
      if(!isset($data['top'])){
        $data['top'] = 0;
      }
      if(!isset($data['column'])){
        $data['column'] = 1;
      }
      if(!isset($data['sort_order'])){
        $data['sort_order'] = 0;
      }
      if(!isset($data['status'])){
        $data['status'] = 1;
      }
      if(!isset($data['keyword'])){
        $data['keyword'] = false;
      }
      if(!isset($data['category_description'])){
        \Dge\Error::write($this,'E002','category_description');
      }else{
        // Make sure every language has a 'name'
        // only needed for the first time, we expect Dutch
        $pdesc = $this->stranslateLocaleFromId($data['category_description']);
        // At least one char as a name, absolute minimum
        if(! isset($pdesc['nl']) || ! isset($pdesc['nl']['name']) || !is_string($pdesc['nl']['name']) || strlen($pdesc['nl']['name']) < 1 ){
          \Dge\Error::write($this,'E001','category_description - nl - name');
          die;
        }
        if(! isset($pdesc['nl']['meta_title']) || !is_string($pdesc['nl']['meta_title'])){
          $pdesc['nl']['meta_title'] = $pdesc['nl']['name'];
        }
        // Fill out a complete desciption
        if(!isset($pdesc['nl']['meta_keyword']) || !is_string($pdesc['nl']['meta_keyword'])){
          $pdesc['nl']['meta_keyword'] = '';
        }
        if(!isset($pdesc['nl']['meta_description']) || !is_string($pdesc['nl']['meta_description'])){
          $pdesc['nl']['meta_description'] = '';
        }
        if(!isset($pdesc['nl']['description']) || !is_string($pdesc['nl']['description'])){
          $pdesc['nl']['description'] = '';
        }
        // Copy language specific things to 'other' languages
        // TODO: do not depend an Dutch standard:
        $data['category_description'] = $this->CopyDescriptions($pdesc['nl']);
      }
      $category_id = $this->model_catalog_category->addCategory($data);
      if(!$category_id){
        \Dge\Error::write($this,'E500','Insert category');
      }
    }
    $old_data = $this->model_catalog_category->getCategory($category_id);
    if(!$old_data){
      \Dge\Error::write($this,'E404');
      die;
    }
    unset($old_data['language_id']);
    unset($old_data['name']);
    unset($old_data['description']);
    unset($old_data['meta_description']);
    unset($old_data['meta_title']);
    unset($old_data['meta_keyword']);
    $old_data = array_merge($old_data,
      array('category_description' => $this->model_catalog_category->getCategoryDescriptions($category_id))
    );
    $old_data = array_merge($old_data,
      array('category_filter' => $this->model_catalog_category->getCategoryFilters($category_id))
    );
    $old_data = array_merge($old_data,
      array('category_store' => $this->model_catalog_category->getCategoryStores($category_id))
    );
    $old_data = array_merge($old_data,
      array('category_layout' => $this->model_catalog_category->getCategoryLayouts($category_id))
    );
    $data = $this->mergeData($old_data, $data);
    if(isset($data['image_b64'])){
      // We have MAIN base64 image data
      // We 'r gonna use this to overwrite the initial image
      $img = $this->safeImages($category_id, $data['image_b64']);
      if($img && count($img) == 1){
        $data['image'] = $img[0]['image'];
      }
      unset($data['image_b64']);
    }
    $this->model_catalog_category->editCategory($category_id, $data);
    $result = array();
    $result['status'] = 'ok';
    $result['category_id'] = $category_id;
    $this->setResponseStatus('200 Ok');
    $this->writeHeaders();
    $this->write($result, 'category');
  }

  // ### delete($dge_id);
  // Delete a category based on id
  private function delete($category_id){
    $test = $this->model_catalog_category->getCategory($category_id);
    if(!$test){
      \Dge\Error::write($this,'E404');
    }else{
      unset($test);
    }
    // OC model delete
    $this->model_catalog_category->deleteCategory($category_id);
    $this->deleteImages($category_id);
    $this->model_catalog_category->repairCategories($category_id);
    $result['status'] = 'deleted';
    $result['category_id'] = $category_id;
    $this->setResponseStatus('200 Ok');
    $this->writeHeaders();
    $this->write($result, 'category');
  }

  // convert an array containing base64 image data
  // to an image and return an array of objects
  // ready for OC to insert
  private function safeImages($category_id, $arr){
    $main_image = false;
    if(is_string($arr)){
      // The filename is something like: data/c/[category_id]_0.jpg
      $main_image = true;
      $arr = array($arr);
    }else{
      return false;
    }
    $bina = array();
    $sort_order = 0;
    // Take the first 2 chars form the id to create a dirname

    $directory = DIR_IMAGE . 'catalog/c';
    if(!is_dir($directory)){
      mkdir($directory, 0777);
    }
    $subdir = substr($category_id.'', 0, 2);
    $directory = $directory.'/'.$subdir;
    if(!is_dir($directory)){
      mkdir($directory, 0777);
    }
    require_once(DGE_APP_PATH.'/Helpers/Binary.php');
    for($i=0;$i<count($arr);$i++){
      list($ct , $data) = \Dge\Binary::b64ToBin($arr[$i]);
      if($ct === 'image/jpeg'){
        $ext = '.jpg';
      }else{
        $ext = '.png';
      }
      $filename = $category_id.'_'.$ext;
      // Not a valid image?
      // Refuse the rest as well
      if($ct && $data){
        $image = 'catalog/c/'.$subdir.'/'.$filename;
         if($ct == 'image/gd'){
          // This is a GD image (converted BMP)
          // Store it as PNG
          imagepng($data, $directory.'/'.$filename, 0);
        }else{
          // Just write, leave it untouched
          $fp = fopen($directory.'/'.$filename, 'w');
          fwrite($fp, $data);
          fclose($fp);
        }
        $bina[] = array('image' => $image,
                        'sort_order' => $sort_order,
                        'category_id' => $category_id
                        );
        $sort_order ++;
      }
    }
    return $bina;
  }

  // Delete ALL category images
  private function deleteImages($category_id){
    $directory = DIR_IMAGE . 'catalog/c';
    $subdir = substr($category_id.'', 0, 2);
    $directory = $directory.'/'.$subdir;
    if(!is_dir($directory)){
      return true;
    }
    $files = glob($directory. '/'.$category_id.'_*');
    if ($files) {
      foreach ($files as $file) {
        unlink ( $file );
      }
    }
    return true;
  }

  // Validate on all values we receive from a POST
  // convert when needed
  protected function validateAndTransform($data){
    $res = true;
    // Cleanup data which we should NOT recieve / overwrite
    // These values are for internal use only
    if(isset($data['image'])){
      unset($data['image']);
    }
    if(isset($data['category_layout'])){
      unset($data['category_layout']);
    }
    foreach ($data as $key => $value) {

      if( ( is_string($value) || is_numeric($value))
          // This fields should be arrays
          // (we can have multiple values)
          && (
               $key === 'category_store'
          )

      ){
        // cast to array
        $data[$key] = array($value);
        $value =  $data[$key];
      }
      $field = $key;
      if ($field === 'category_store') $field = 'store_id';
      if ($field === 'category_filter') $field = 'filter_id';
      if($key === 'category_description'){
       list($err, $new_ar) = $this->stranslateLocaleFromISO2($value);
        if($err){
          return array( 'category_description : '.$err, false);
        }else{
          $data[$key] = $new_ar;
          $res = true;
        }
      }else if(is_array($value) && ! \Dge\App::is_assoc($value) ){
        // Value is an indexed array
        // check every single value
        $res = $this->validateArray($field, $value);
        if(!$res){
          return array($key, false);
        }
      }else{
        $res = $this->validate($field, $value);
      }
      if(!$res){
        return array($key, false);
      }
    }
    return array(false, $data);
  }

  //  return TRUE on success
  protected function validate($type, $value){
    $res = false;
    switch ($type) {
      // Ints
      case 'parent_id':
      case 'sort_order':
      case 'language_id':
      case 'store_id':
      case 'category_id':
      case 'filter_id':
      case 'column':
        $res = ((int)$value.'' == $value);
        break;
      // tiny ints
      case 'top':
      case 'status':
      case 'required':
        $res = ((int)$value == 0 || (int)$value == 1);
        break;
      case 'image':
      case 'keyword':
      case 'meta_description':
      case 'meta_keyword':
        $res = (strlen($value.'') < 256);
        break;
      case 'name':
      case 'meta_title':
        $res = (strlen($value.'') > 1 && strlen($value.'') < 101);
        break;
      case 'description':
      case 'tag':
      case 'text':
      case 'image_b64':
        $res = (is_string($value) || is_numeric($value));
        break;
      case 'created':
      case 'date_added':
      case 'date_modified':
      case 'date_start':
      case 'date_end':
        // yyyy-mm-dd
        $pattern = '/^\d{4}-\d{2}-\d{2}$/';
        $test = preg_match ( $pattern , $value );
        if($test === 1){
          $res = true;
        }else{
          $res = false;
        }
        break;
      default:
        # code...
        break;
    }
    return $res;
  }

  protected function validateArray($field, $arr){
    $res = true;
    for($i = 0; $i < count($arr); $i++){
      $res = $this->validate($field, $arr[$i]);
      if(!$res){
        return false;
      }
    }
    return true;
  }

  protected function getProductIdsByCategory($category_id){
    $this->load->model('catalog/product');
    return $this->model_catalog_product->getProductsByCategoryId($category_id);
  }

  protected function getDgeProductReferenceData($product_id){
    $dgeCatalogus = \Dge\App::loadModel('product');
    return $catalogus->getDgeReference($product_id);
  }

}
