<?php
/*
This file is part of OCAPI
Opencart HTTP(s) XML/JSON API

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues]

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Dge;

class Controller {

  const DGE_BASE_NAME =  'Dge Driver (v2)';

  private $contentType = false;
  private $status = '200 Ok'; /* becomes HTTP/1.1 200 ok */
  private $headers = array();

  protected $registry;

  function __construct($ct, $registry) {
    $this->registry = $registry;
    $this->contentType = $ct;
    \Dge\App::chunkStart();
  }

  public function __get($key) {
    return $this->registry->get($key);
  }

  public function __set($key, $value) {
    $this->registry->set($key, $value);
  }

  public function setResponseStatus($str){
    $this->status = $str;
  }

  public function addResponseHeader($key, $value){
    $this->headers[$key] = $value;
  }

  public function writeHeaders(){
    $this->addResponseHeader('X-Powered-By', 'OC '.self::DGE_BASE_NAME);
    $this->addResponseHeader('Content-Type', $this->contentType);
    \Dge\App::chunkHeader($this->status, $this->headers);
  }

  public function write($ob,$nodename = 'node'){
    if(is_object($ob)){
      $ob = get_object_vars($ob);
    }
    if(is_string($ob)){
      $ob = array($ob);
    }
    if($this->contentType === 'application/xml; charset=utf-8'){
      $msg = self::toXml($ob,'response', $nodename);
    }else{
      $msg = json_encode($ob, true);
    }
    \Dge\App::chunk($msg);
    \Dge\App::chunkFooter();
  }

  public static function toXml($obj, $root_node = 'response', $node_name ='node') {
    $data = array();
    $data[$node_name] = $obj;
    $xml = \Array2XML::createXML($root_node, $data);
    return $xml->saveXML();
  }

  protected function getPostData(){
    $data = array();
    $postdata = file_get_contents("php://input");
    if(strlen($postdata)<2){
      return false;
    }

    /* filter NON utf-8 chars, but don't die on it */
    $postdata = iconv('UTF-8', 'UTF-8//IGNORE', $postdata);

    foreach (getallheaders() as $key => $value){
      // Sometimes (ngix) the content-type header becomes content_type
      if( strtolower($key) === 'content-type' || strtolower($key) === 'content_type'){
        $ct = strtolower($value);
        if(substr_count($ct ,'json') === 1){
          $data = json_decode($postdata, true);
          return $data;
        }else if(substr_count($ct ,'xml') === 1){
          // Easy does it:
          // normalize via json
          $data = json_encode(simplexml_load_string($postdata));
          $data = json_decode($data, true);
          return $data;
        }
        return false;
      }
    }
  }

  // Read the params and return the ID
  // which is/should allways param[0]
  protected function getIdParam($params){
    if(count($params) !== 1 || $params[0] == ''){
      return false;
    }
    $id = (int) $params[0];
    if($id < 1){
      return false;
    }
    return $id;
  }

  protected function stranslateLocaleFromId($ar){
    // arr is an array where the index is a language_id
    $res = array();
    $langs = $this->config->get('languages');
    if(count($langs) === 1){
      $k = key($langs);
      $k2 = key($ar);
      $res['nl'] = $ar[$k2];

    }else{
      foreach ($langs as $index => $lang) {
        if(array_key_exists ( $lang['language_id'] , $ar )){
          $res['nl'] = $ar[$lang['language_id']];
        }
      }
    }
    return $res;
  }

  // We translate the ISO2 language code to a
  // language_id INDEX
  // and validate each lang
  // however when there is only ONE language
  // we expect we return THAT language
  protected function stranslateLocaleFromISO2($ar){
    // arr is an array where the index is a language_code
    // we also clean on language code
    // and replace language code with language_id

    $error = false;
    $res = array();
    $langs = $this->config->get('languages');
    if(count($langs) === 1 && array_key_exists ( 'nl' , $ar )){
      $k = key($langs);
      // Force dutch as language () despite the
      // language in use on Opencart
      $res[$langs[$k]['language_id']] = $ar['nl'];
    }else{
      foreach ($langs as $index => $lang) {
        if($lang['code'] == 'nl-nl'){
          $lang['code'] = 'nl';
        }
        if(array_key_exists ( $lang['code'] , $ar )){
          // And do a check on values
          
          foreach ($ar['nl'] as $key => $value) {

            $valid = $this->validate($key,$value);
            if(!$valid){
              $error = $lang['code'] . ' : ' .$key;
              return array($error, false);
            }
          }
          $res[$lang['language_id']] = $ar[$lang['code']];
        }
      }
    }

    if(count($res) < 1){
      $error = 'No locale found';
      return array($error,$res);
    }else{
      return array($error,$res);
    }
  }
  // Get a country ID by iso2 name
  protected function getCountryByIso2($is) {
    $is = strtoupper($is);
    $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "country WHERE iso_code_2 = '" . $this->db->escape($is) . "'");
    return $query->row;
  }

  // Get a country ID by iso2 name
  protected function getZoneByName($country_id, $name) {
    $name = strtolower($name);
    $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "zone WHERE country_id = '" . (int)$country_id . "' AND LCASE(name) = '".$this->db->escape($name)."' LIMIT 1");

    return $query->row;
  }

  // #### mergeData($old_data, $new_data)
  // We have VALIDATED post data
  // here w 'r gonna merge the data
  // we only overwrite what is given
  // and merge it with the original
  protected function mergeData($old_data, $new_data){
    $new = array_replace_recursive($old_data, $new_data);
    return $new;
  }

  // Fill the language specific product names
  // in case they are missing
  public function CopyDescriptions($description_ob){
    $langs = $this->config->get('languages');
    $ar = array();
    foreach ($langs as $index => $lang) {
      $ar[$lang['language_id']]= $description_ob;
    }
    return $ar;
  }
}
