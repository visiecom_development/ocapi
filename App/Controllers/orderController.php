<?php
/*
This file is part of OCAPI
Opencart HTTP(s) XML/JSON API

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues]

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Dge;

class orderController extends \Dge\Controller {

  function __construct($params, $ct, $reg) {
    parent::__construct($ct, $reg);
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {
      case 'GET':
      case 'POST':
      case 'PUT':
        if($method === 'PUT'){
          $method = 'POST';
        }
        $task = 'index';
        if(count($params) === 1 && $params[0] === 'statuslist'){
          $task = 'statuslist';
        }else if(count($params) === 2 && $params[0] === 'delete'){
          $method = strtoupper(array_shift($params));
        }else if(count($params) === 2 && $params[0] === 'status'){
          array_shift($params);
          $task = 'status';
        }
        break;
      case 'DELETE':
        $task = 'index';
        break;
      default:
        $task = '';
        break;
    }

    $this->handleTask($task, $params, $method);
  }

  private function handleTask($task, $params, $method = 'GET'){
    switch ($task.'_'.$method) {
      case 'index_GET':
        $order = \Dge\App::loadModel('order');
        if($method === 'GET'){
          $id = $this->getIdParam($params);
          if($id){
            $data = $order->get($id);
            if($data){
              $settings = \Dge\App::getSettings();
              $pending = (int)$settings['order_status']['pending'];
              $paid = (int)$settings['order_status']['paid'];
              $complete = (int)$settings['order_status']['complete'];
              if((int)$data['order_status_id'] === $paid){
                $data['status'] = 'paid';
              }else if((int)$data['order_status_id'] === $pending){
                $data['status'] = 'pending';
              }else if((int)$data['order_status_id'] === $complete){
                $data['status'] = 'complete';
              }else{
                $data['status'] = 'unkown';
              }
              $this->writeHeaders();
              $this->write($data, 'order');
            }else{
              \Dge\Error::write($this,'E404');
            }
            die;
          }
          // E.G: http://.../opencart/v1/order.xml?start=0&limit=3&order=desc&sort=date_added
          $filter = array();
          isset($_REQUEST['start']) ? $filter['start'] = (int)$_REQUEST['start'] : $filter['start'] = 0;
          isset($_REQUEST['limit']) ? $filter['limit'] = (int)$_REQUEST['limit'] : $filter['limit'] = 20;
          isset($_REQUEST['order']) ? $filter['order'] = strtoupper($_REQUEST['order']) : $e = NULL;
          // OC-36 we need orders with 2 statussen
          // pending and paid
          // sort by methods
          $sort = array();
          $sort['order_id'] = 'o.order_id';
          $sort['customer'] = 'customer';
          $sort['status'] = 'status';
          $sort['date_added'] = 'o.date_added';
          $sort['date_modified'] = 'o.date_modified';
          $sort['total'] = 'o.total';
          if(isset($_REQUEST['sort']) && array_key_exists($_REQUEST['sort'], $sort)){
            $filter['sort'] = $sort[$_REQUEST['sort']];
          }
          $data = $order->getCollection($filter);
          $this->writeHeaders();
          $this->write($data, 'order');
        }
        break;
      case 'status_POST':
      case 'status_PUT':
        // Status change for an order
        $id = $this->getIdParam($params);
        $data = $this->getPostData();
        if(!$data){
          \Dge\Error::write($this,'E400', 'Posted data is not readable');
        }
        if(!$id || $id < 1){
          \Dge\Error::write($this,'E400', 'Missing id');
        }
        $this->status($id,$data);
        break;
      default:
        \Dge\Error::write($this,'E501', 'Not Implemented');
        break;
    }
  }

  private function status($id, $data){
    // Change the status for an order
    //
    // needed:
    // - order_status_id
    // - notify (optional) 1 || 0
    // - comment (plain text)
    $order = \Dge\App::loadModel('order');
    list($field_error, $data) = $order->validateAndTransform($data);
    if($field_error){
      \Dge\Error::write($this,'E001',$field_error);
      die;
    }
    $tst = $order->get($id);
    if(!$tst){
      \Dge\Error::write($this,'E404');
      die;
    }else{
      unset($tst);
    }
    $settings = \Dge\App::getSettings();
    $status_id = $order->getStatusId($data["order_status"]);
    if(!$status_id){
      \Dge\Error::write($this,'E001','Missing valid order status');
      die;
    }
    $data['order_status_id'] = $status_id;
    unset($data['order_status']);
    $order->addOrderHistory($id, $data);
    $result['status'] = 'status update';
    $result['order_id'] = $id;
    $this->setResponseStatus('200 Ok');
    $this->writeHeaders();
    $this->write($result, 'order');
  }
}
