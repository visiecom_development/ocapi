<?php
/*
This file is part of OCAPI
Opencart HTTP(s) XML/JSON API

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues]

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Dge;
define('DGE_APP_PATH', realpath(dirname(__FILE__)));

require_once('Helpers/Error.php');
require_once('Mysql/Db.php');
require_once('Helpers/Helper.php');
require_once('Helpers/Array2XML.php');
require_once('Controllers/Controller.php');
require_once('Models/Model.php');
require_once('Helpers/AclClient.php');

class App {

  // [Db](Db.html)
  private static $db = false; /* $db instance */
  private static $registery = false; /* opencart registry object */
  private static $settings = false; /* OCPAI settings.json */

  // #### \Dge\App::loadController($controller, $params , $ct, $reg)
  // Dynamically load a controller
  // ##### Params:
  // * $controller, string controller name
  // * $params, array url spliced on '/'
  // * $ct, string Content-Type, in what format do we output
  public static function loadController($controller, $params , $ct, $reg){
      $object = false;

      $fp = realpath(dirname(__FILE__)).'/Controllers/'.$controller.'Controller.php';

      if(!is_file($fp)){
        /* not found */
        self::chunkHeader('404 Not Found', array());
        self::chunkFooter();
      }
      require_once($fp);
      $class = 'Dge\\'.$controller.'Controller';
      try {
        self::$registery = $reg;
        $object = new $class($params, $ct, $reg);
      } catch (Exception $e) {
        /* error in object initialization */
        self::chunkHeader('500 Internal Server Error', array());
        self::chunkFooter();
      }
      return $object;
  }

  // #### \Dge\App::loadModel()
  // Dynamically load a model based on $model (string) name
  public static function loadModel($model){
      $object = false;
      $fp = realpath(dirname(__FILE__)).'/Models/'.$model.'Model.php';
      if(!is_file($fp)){
        /* Not found */
       return false;
      }
      require_once($fp);
      $class = 'Dge\\'.$model.'Model';
      $object = new $class(self::$registery);
      return $object;
  }

  // #### \Dge\App::setDb()
  // Open and set a global MYSQLI[Db](Db.html)
  // ##### Params:
  // * $host , string, ip or domain name (null when there is a socket param)
  // * $user , string, username
  // * $password , string, password
  // * $database , string, database
  // * $port , int, port number
  // * $socket , string, path to mysql socket
  public static function setDb($host, $user, $password, $database = null, $port = null, $socket = null){
    if(self::$db){
      throw new \Exception("DB allready set", 1);
    }
    self::$db = new \Mysql\Db($host, $user, $password, $database, $port, $socket);
    self::$db->check();
  }

  // #### \Dge\App::getDb()
  // Get the database wrapper object
  public static function getDb(){
    if(! self::$db){

      throw new \Exception('No database connection found', 1);
    }
    return self::$db;
  }

  // #### \Dge\App::clear()
  // Unsset the DB object (and other objects)
  //  so those may be garbage collected
  public static function clear(){
    // CLEAN UP REFS
    self::$db = null;
    self::$registery = null;
  }

  // #### \Dge\App::is_assoc($a)
  // Determine if an array is a tuple / plist
  public static function is_assoc($a){
        // return is_array($var) && array_diff_key($var,array_keys(array_keys($var)));
    return is_array( $a ) && ( count( $a ) !== array_reduce( array_keys( $a ), create_function( '$a, $b', 'return ($b === $a ? $a + 1 : 0);' ), 0 ) );
  }

  // #### \Dge\App::getOpenCartEndpoint($div)
  //
  // Param $div seperates the url so we can construct an endpoint
  //
  // e.g.
  // http:://www.myshop.nl/myendpoint/controller/params
  // or
  // http:://www.domain.nl/webshop/myendpoint/controller/params
  // $div = 'myendpoint'
  public static function getEndpoint($div){
    $webshopIdentifier = '';
    $urlPrefix = "";
    switch ($_SERVER["SERVER_PORT"]){
      case 443:
        $urlPrefix = "https://";
        break;
      default:
        $urlPrefix = "http://";
        break;
    }
    if(isset($_SERVER['HTTP_X_FORWARDED_HTTPS'])){
        $urlPrefix = $_SERVER['HTTP_X_FORWARDED_HTTPS']."://";
    }

    $url = $urlPrefix.$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
    $test = preg_split ( '/'.$div.'/' ,$_SERVER["REQUEST_URI"] );
    if(count($test) >= 1 ){
      $webshopIdentifier = $urlPrefix.$_SERVER["HTTP_HOST"].$test[0].$div.'/';
    }
    return $webshopIdentifier;
  }

  // #### \Dge\App::getRequestDetails()
  // * Determine the requested content-type to return
  // * Find out which controller to use
  // * Get the params
  # Param $div separates the url so we can construct an endpoint
  # @see self::getEndpoint
  public static function getRequestDetails($div){
    $path = '';
    if(isset($_SERVER['REQUEST_URI'])){
      $path = explode('?', $_SERVER['REQUEST_URI']);
      $path = $path[0];
    }

    $ext = isset($path) ? pathinfo($path, PATHINFO_EXTENSION): false;
    // Determine content-type by extension
    $params = array();
    $contenttype = false;

    if($ext){
      // Clean path info, remove extension
      $path = str_replace('.'.$ext,'',$path);
      if($ext === 'json'){
        $contenttype = 'application/json; charset=utf-8';
      }else if($ext === 'xml'){
        $contenttype = 'application/xml; charset=utf-8';
      }
    }
    // Determine content-type by ACCEPT request header
    if (!$contenttype && isset($_SERVER['HTTP_ACCEPT'])){
      $indx = strpos ( $_SERVER['HTTP_ACCEPT'] , 'application/xml; charset=utf-8');
      $indxj = strpos ( $_SERVER['HTTP_ACCEPT'] , 'application/json; charset=utf-8');
      if($indx >= 0 || $indxj >= 0){
        if($indxj && $indxj >= 0){
          $contenttype = 'application/json; charset=utf-8';
        }else{
          $contenttype = 'application/xml; charset=utf-8';
        }
      }
    }else if(!$contenttype){
      // Fallback to JSON
      $contenttype = 'application/json; charset=utf-8';
    }
    // Get optional params
    $params = explode('/',$path) ;
    $indx = array_search($div, $params);
    $params = array_slice($params, $indx+1);

    // Dispatch controller
    $controller = array_shift($params);

    if(!$controller) $controller = 'index';
    return array($controller, $params, $contenttype);
  }

  // #### \Dge\App::getSettings()
  // Load a config file and cache it in ::$config
  public static function getSettings(){
    if(!self::$settings){
      $file = realpath(dirname(__FILE__).'/../').'/settings.json';
      if(is_file($file)){
        $data = file_get_contents($file);
        if($data){
          self::$settings = json_decode($data, true);
        }
      }
    }
    return self::$settings;
  }

  // ### Chunked data
  //
  // [Chunked_transfer_encoding](http://en.wikipedia.org/wiki/Chunked_transfer_encoding)
  // is what we use. This way we may output large parst of data without building it up
  // in memory.

  // #### \Dge\App::chunkStart()
  // This will empty the buffer.
  public static function chunkStart(){
    for ($i = 0; $i < ob_get_level(); $i++){ob_end_flush();}
    ob_implicit_flush(1);
  }

  // #### \Dge\App::chunkStart()
  // Start outputting data
  //
  // ##### Params:
  // * $status , string status code e.g. '200 ok'
  // * $headers, key value header fields
  public static function chunkHeader($status, $headers){
    header('HTTP/1.1 '.$status);
    header("Transfer-Encoding: chunked");
    // header('Content-type: '.$ct);
    foreach ($headers as $key => $value) {
      # code...
      header($key.": ".$value);
    }
    flush();
  }

  // #### \Dge\App::chunk($str)
  // Output a string as chunked data
  public static function chunk($str){
    printf("%x\r\n%s\r\n", strlen($str), $str);
    flush();
  }

  // #### \Dge\App::chunkFooter($str)
  // Close the output stream
  public static function chunkFooter(){
    echo(0);
    echo "\r\n";
    echo "\r\n";
    flush();
    self::clear();
    die;
  }
}

// Somehow this function 'getallheaders' does not exists on all platforms.
if (!function_exists('getallheaders')) {
  function getAllHeaders() {
    $out = array();
    foreach($_SERVER as $key=>$value) {
      if (substr($key,0,5)=="HTTP_") {
        $key=str_replace(" ","-",ucwords(strtolower(str_replace("_"," ",substr($key,5)))));
        $out[$key]=$value;
      }else{
        $out[$key]=$value;
      }
    }
    return $out;
  }
}
