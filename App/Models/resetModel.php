<?php
/*
This file is part of OCAPI
Opencart HTTP(s) XML/JSON API

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues]

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Dge;

class resetModel extends \Dge\Model {

  private $languages = false;

  function __construct($reg) {
    parent::__construct($reg);
  }
  function __destruct() {
    parent::__destruct();
  }

  // reset
  function reset(){

    //  tables to empty
    // DELETE FROM oc2.oc_category where 1;
    // DELETE FROM oc2.oc_category_description where 1
    $tables = array();
    $tables[] = "`" . DB_PREFIX . "category`";
    $tables[] = "`" . DB_PREFIX . "category_description`";
    $tables[] = "`" . DB_PREFIX . "attribute`";
    $tables[] = "`" . DB_PREFIX . "attribute_description`";
    $tables[] = "`" . DB_PREFIX . "attribute_group`";
    $tables[] = "`" . DB_PREFIX . "attribute_group_description`";
    $tables[] = "`" . DB_PREFIX . "category_filter`";
    $tables[] = "`" . DB_PREFIX . "category_path`";
    $tables[] = "`" . DB_PREFIX . "category_to_layout`";
    $tables[] = "`" . DB_PREFIX . "category_to_store`";
    $tables[] = "`" . DB_PREFIX . "filter`";
    $tables[] = "`" . DB_PREFIX . "filter_description`";
    $tables[] = "`" . DB_PREFIX . "filter_group`";
    $tables[] = "`" . DB_PREFIX . "filter_group_description`";
    $tables[] = "`" . DB_PREFIX . "manufacturer`";
    $tables[] = "`" . DB_PREFIX . "manufacturer_to_store`";
    $tables[] = "`" . DB_PREFIX . "option`";
    $tables[] = "`" . DB_PREFIX . "option_description`";
    $tables[] = "`" . DB_PREFIX . "option_value`";
    $tables[] = "`" . DB_PREFIX . "option_value_description`";
    $tables[] = "`" . DB_PREFIX . "product`";
    $tables[] = "`" . DB_PREFIX . "product_attribute`";
    $tables[] = "`" . DB_PREFIX . "product_description`";
    $tables[] = "`" . DB_PREFIX . "product_discount`";
    $tables[] = "`" . DB_PREFIX . "product_filter`";
    $tables[] = "`" . DB_PREFIX . "product_image`";
    $tables[] = "`" . DB_PREFIX . "product_option`";
    $tables[] = "`" . DB_PREFIX . "product_option_value`";
    $tables[] = "`" . DB_PREFIX . "product_related`";
    $tables[] = "`" . DB_PREFIX . "product_reward`";
    $tables[] = "`" . DB_PREFIX . "product_special`";
    $tables[] = "`" . DB_PREFIX . "product_to_category`";
    $tables[] = "`" . DB_PREFIX . "product_to_layout`";
    $tables[] = "`" . DB_PREFIX . "product_to_store`";
    $tables[] = "`" . DB_PREFIX . "review`";
    $tables[] = "`" . DB_PREFIX . "url_alias`";
    for($i=0;$i<count($tables);$i++){
      $sql = "DELETE FROM ".$tables[$i]." WHERE 1";
      $query = $this->db->query($sql);
      $sql = "ALTER TABLE ".$tables[$i]." AUTO_INCREMENT = 10";
      $query = $this->db->query($sql);
    }
    $sql = "SHOW TABLES LIKE '". DB_PREFIX ."dge_product_reference' ";
    $query = $this->db->query($sql);
    if(count($query->rows)>0){
      $sql = " DELETE FROM `". DB_PREFIX ."dge_product_reference` WHERE 1";
      $query = $this->db->query($sql);
    }else{
      $sql = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX."dge_product_reference` (  `product_id` int(11) NOT NULL, `catalogus_id` int(11) NOT NULL, `unit_id` int(11) NOT NULL, `unit_description` varchar(64) NOT NULL, PRIMARY KEY (`product_id`) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
      $query = $this->db->query($sql);
    }
       // DELETE IMAGE DIR(s)
    $dir = DIR_IMAGE . 'catalog/p';
    $this->rrmdir($dir);

    $dir = DIR_IMAGE . 'catalog/c';
    $this->rrmdir($dir);

    $dir = DIR_IMAGE . 'cache/catalog/p';
    $this->rrmdir($dir);

    $dir = DIR_IMAGE . 'cache/catalog/c';
    $this->rrmdir($dir);

    return 1;
  }

  function rrmdir($dir){
   if (is_dir($dir)) {
     $objects = scandir($dir);
     foreach ($objects as $object) {
       if ($object != "." && $object != "..") {
         if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); else unlink($dir."/".$object);
       }
     }
     reset($objects);
     rmdir($dir);
   }
  }
}
