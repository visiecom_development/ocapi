<?php
/*
This file is part of OCAPI
Opencart HTTP(s) XML/JSON API

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues]

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 CREATE TABLE IF NOT EXISTS `oc_dge_product_reference` (
  `product_id` int(11) NOT NULL,
  `catalogus_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `unit_desciption` varchar(64) NOT NULL,
  PRIMARY KEY (`product_id`,`catalogus_id`, unit_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
*/
namespace Dge;

class productModel extends \Dge\Model {

  private $languages = false;

  function __construct($reg) {
    parent::__construct($reg);
  }
  function __destruct() {
    parent::__destruct();
  }

  // #### addDgeReference($product_id, $catalogus_id, $unit_id, $unit_description)
  //  ##### params:
  // * $product_id int opencart product id
  // * $catalogus_id int dge catalogus id
  // * $unit_id int dge 'verkoopeenheid' id
  // * $unit_description string 64 'e.g.: rol, pallet, doos, '

  public function addDgeReference($product_id, $catalogus_id, $unit_id, $unit_description){
    $sql = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX."dge_product_reference` (  `product_id` int(11) NOT NULL, `catalogus_id` int(11) NOT NULL, `unit_id` int(11) NOT NULL, `unit_description` varchar(64) NOT NULL, PRIMARY KEY (`product_id`) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
    $query = $this->db->query($sql);

    $sql = "DELETE FROM `".DB_PREFIX."dge_product_reference` WHERE `catalogus_id` = ".intval($catalogus_id)." and `unit_id` = ".intval($unit_id)."";
    $query = $this->db->query($sql);
    $sql = "DELETE FROM `".DB_PREFIX."dge_product_reference` WHERE `product_id` = ".intval($product_id);
    $query = $this->db->query($sql);


    $sql = "INSERT INTO `".DB_PREFIX."dge_product_reference` (`product_id`,`catalogus_id`,`unit_id`,`unit_description`) VALUES (".intval($product_id).",".intval($catalogus_id).",".intval($unit_id).",'".$this->db->escape($unit_description)."')";
    $query = $this->db->query($sql);
  }

  // #### delDgeReference($product_id)
  // Delete dge references based on an opencart id
  //
  // @todo : This should also be called when a product is deleted via the Opencart
  //         UI. (By a trigger)
  public function delDgeReference($product_id){
    $sql = "DELETE FROM `".DB_PREFIX."dge_product_reference` WHERE `product_id` = ".intval($product_id);
    $query = $this->db->query($sql);
  }

  // #### getDgeReference($product_id)
  // * $product_id int opencart product_id
  public function getDgeReference($product_id){
    $sql = "SELECT * FROM `".DB_PREFIX."dge_product_reference` WHERE `product_id` = ".intval($product_id);
    $query = $this->db->query($sql);
    if($query->row){
      return $query->row;
    }else{
      return false;
    }
  }

  public function getOnEan($ean){
    $sql = "SELECT product_id, isbn AS unit FROM `".DB_PREFIX."product` WHERE `ean` = '".$ean."'";
    $query = $this->db->query($sql);
    return $query->rows;
  }

  // #### getDgeReference($product_id)
  // * $product_id int opencart product_id
  public function getReferenceByCatId($cat_id){
    $sql = "SHOW TABLES LIKE '". DB_PREFIX ."dge_product_reference' ";
    $query = $this->db->query($sql);
    if(count($query->rows)=== 0){
      $sql = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX."dge_product_reference` (  `product_id` int(11) NOT NULL, `catalogus_id` int(11) NOT NULL, `unit_id` int(11) NOT NULL, `unit_description` varchar(64) NOT NULL, PRIMARY KEY (`product_id`) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
      $query = $this->db->query($sql);
    }

    $sql = "SELECT * FROM `".DB_PREFIX."dge_product_reference` WHERE `catalogus_id` = ".intval($cat_id);
    $query = $this->db->query($sql);
    return $query->rows;
  }

  public function getProductsWithOptions(){
    $sql = "SELECT distinct product_id FROM ".DB_PREFIX."product_option";
    $query = $this->db->query($sql);
    return $query->rows;
  }

  // #### getJan($product_id)
  // In the 'rare' case an existing opencart webshop should be migrated to DGE
  // the JAN field is used to enter a dge catalogus id. Via the JAN field we will be able
  // to reference an opencart product id with a dge catalogus id.
  // @see categoryController::index() line 125.
  public function getJan($product_id){
    $sql = "SELECT jan FROM `".DB_PREFIX."product` WHERE `product_id` = ".intval($product_id);
    $query = $this->db->query($sql);
    if($query->row){
      return $query->row['jan'];
    }else{
      return false;
    }

  }

  // #### taxRate2TaxClassId($indication)
  // return a tax_class_id
  // we have the following VAT codes:
  //
  // + high
  // + low
  // + none
  //
  // The conversion is in settings.json
  public function taxRate2TaxClassId($indication){
    $settings = \Dge\App::getSettings();
    if(!$settings){
      return false;
    }
    $tax_class_id = false;
    if(array_key_exists ($indication, $settings["tax_class"])){
      $tax_class_id = $settings["tax_class"][$indication];
    }
    if($tax_class_id === false){
      return false;
    }
    if($tax_class_id == 0){
      return $tax_class_id;
    }
    $this->load->model('localisation/tax_class');
    $classes = $this->model_localisation_tax_class->getTaxClasses();
    for($i=0;$i<count($classes);$i++){
      if($tax_class_id == $classes[$i]['tax_class_id']){
        return $classes[$i]['tax_class_id'];
      }
    }
    return false;
  }

  // #### getLengthClass($indication)
  // return a length_class
  // The following lengths should be defined
  //
  // - cm
  // - mm
  // - in
  //
  public function getLengthClass($indication){

    $length_class = false;

    // Data is an array

    $this->load->model('localisation/length_class');
    $classes = $this->model_localisation_length_class->getLengthClasses();
    for($i=0;$i<count($classes);$i++){
      if($indication === strtolower($classes[$i]['unit'])){
        $length_class = $classes[$i];
        break;
      }
    }
    return $length_class;
  }

  // #### getWeightClass($indication)
  // return a weight_class
  // The following weights should be defined
  // - kg
  // - g
  // - lb
  // - oz
  public function getWeightClass($indication){
    // g. kgs
    $weight_class = false;
    $this->load->model('localisation/weight_class');
    $classes = $this->model_localisation_weight_class->getWeightClasses();
    for($i=0;$i<count($classes);$i++){
      if($indication === strtolower($classes[$i]['unit'])){
        $weight_class = $classes[$i];
        break;
      }
    }

    return $weight_class;
  }
}
