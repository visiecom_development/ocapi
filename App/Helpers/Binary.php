<?php
/*
This file is part of OCAPI
Opencart HTTP(s) XML/JSON API

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues]

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * Static Binary helper class
 *
 * - Convert base64 to GD image data
 * - Get the binary mimetype
 * - Convert binary (bmp -> png)
 */


namespace Dge;

class Binary {

  // Convert base64 string to data
  // returns array($contentType, $binary)
  static function b64ToBin($str){
    $data = base64_decode($str);
    $ct = self::contentTypeFormbinary($data);

    // We only accept jpg, png (for now)
    if(! ($ct == 'image/jpeg' || $ct == 'image/png')){
      return array(false, false);
    }
    return array($ct, $data);
  }

  static function contentTypeFormbinary($data) {
      if (
          !preg_match(
              '/\A(?:(\xff\xd8\xff)|(GIF8[79]a)|(\x89PNG\x0d\x0a)|(BM)|(\x49\x49(?:\x2a\x00|\x00\x4a))|(FORM.{4}ILBM)|(%PDF-))/',
              $data, $hits
          )
      ) {
          return 'application/octet-stream';
      }
      static $type = array (
          1 => 'image/jpeg',
          2 => 'image/gif',
          3 => 'image/png',
          4 => 'image/x-windows-bmp',
          5 => 'image/tiff',
          6 => 'image/x-ilbm',
          7 => 'application/pdf'
      );
      return $type[count($hits) - 1];
  }
}
