<?php
/*
This file is part of OCAPI
Opencart HTTP(s) XML/JSON API

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues]

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Static Error Helper Class
//
// Single Place to keep and collect the Various Errors


namespace Dge;

class Error {

  // #### 400 Bad Request (E400)
  static $E400 = '(POST/GET) Request is not recognized';

  // #### 412 Precondition Failed (E0XX)

  // ####  E001
  // A posted json/xml field does not have the proper value
  static $E001 = 'Post Field Value';

  // #### E002
  // A posted json/xml collection is missing a value
  static $E002 = 'Missing Post Field';

  // #### E003
  // A parameter is missing in the request
  static $E003 = 'Request Paramater Missing';

  // #### E004
  // A parameter does not have a proper value
  static $E004 = 'Request Paramater Value';

  // ####  ## 404 Not found (E404)
  // Not Found
  static $E404 = 'Not found what i\'m looking for';

  // #### 500 Internal server error (E5XX)
  // #### E500
  // Somehow, despite all checks, something went wrong
  static $E500 = 'Undetermined Server Error';

  // #### E501
  // Just no handler available
  static $E501 = 'Undetermined Server Error';


  // #### Write an error via the controller
  public static function write($ctr, $ERRNO, $str = ''){

    $resp = array();

    $resp['ERRNO'] = $ERRNO;
    $resp['message'] = '';
    if(isset(self::$$ERRNO)){
      $resp['message'] = self::$$ERRNO;
    }

    $resp['description'] = $str;


    switch ($ERRNO) {
      case 'E400':
        $status = '400 Bad Request';
        break;
      case 'E001':
      case 'E002':
      case 'E003':
      case 'E004':
        $status = '406 Not Acceptable';
        # code...
        break;

      case 'E404':
        $status = '404 Not found';
        break;
      case 'E500':
        $status = '500 Internal server error';
        break;
      default:
        $status = '501 Not Implemented';
        break;
    }

    $ctr->setResponseStatus($status);
    $ctr->writeHeaders();
    $ctr->write($resp, 'error');
    die;
  }
}
