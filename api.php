<?php
# vim: ts=2 sw=2 et
ini_set('display_errors', 'Off');
// Disable compression
// Enable buffering / flushing
if (function_exists('apache_setenv')) {
  apache_setenv('no-gzip', 1);
}
ini_set('output_buffering',4096);
ini_set('zlib.output_compression', 0);
ini_set('implicit_flush', 1);
/*
OCAPI
Opencart HTTP(s) XML/JSON API

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues]

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
ob_start();
// Version
define('VERSION', '2.3.0.3');
define('API_VERSION', 'v2.3');
error_reporting(E_ERROR | E_WARNING | E_PARSE);


// Load API framework
require_once('App/App.php');
require_once('Developer/Log.php');
require_once('App/Helpers/AclClient.php');

$settings = \Dge\App::getSettings();
if(!$settings){
  header('HTTP/1.1 500 Internal server error');
  echo('ERROR Unable to load settings');
  die;
}

// Configuration
if (file_exists($settings['admin_config'])) {
	// Load the opencart admin config.php
	require_once($settings['admin_config']);
}else{
	echo('ERROR finding settings');
	die;
}


// $config = new Config();
// $config->load('default');
// $config->load($settings['admin_config']);
// $registry->set('config', $config);

if (!defined('DIR_APPLICATION')) {
	// Not installed
  header('HTTP/1.1 500 Internal server error');
  echo('ERROR unkown dir application');
	die;
}
// Set ACL cache dir
\Helper\AclClient::$cache_dir = DIR_CACHE.'acl';
// Enable debugging
if($settings['debug'] === true){
    error_reporting(E_ALL);
    Developer\Log::setDebugFile('../system/storage/logs/debug.log');
    Developer\Log::enableDebugging();
    // e.g.:
    // \Developer\Log::debug([$obj, string, int etc.])
}
// Startup

function modification($filename) {
  if (!defined('DIR_CATALOG')) {
    $file = DIR_MODIFICATION . 'catalog/' . substr($filename, strlen(DIR_APPLICATION));
  } else {
    $file = DIR_MODIFICATION . 'admin/' .  substr($filename, strlen(DIR_APPLICATION));
  }

  if (substr($filename, 0, strlen(DIR_SYSTEM)) == DIR_SYSTEM) {
    $file = DIR_MODIFICATION . 'system/' . substr($filename, strlen(DIR_SYSTEM));
  }

  if (is_file($file)) {
    return $file;
  }

  return $filename;
}

// Autoloader
if (file_exists(DIR_SYSTEM . '../../vendor/autoload.php')) {
  require_once(DIR_SYSTEM . '../../vendor/autoload.php');
}

function library($class) {
  $file = DIR_SYSTEM . 'library/' . str_replace('\\', '/', strtolower($class)) . '.php';

  if (is_file($file)) {
    include_once(modification($file));

    return true;
  } else {
    return false;
  }
}

spl_autoload_register('library');
spl_autoload_extensions('.php');

// Engine
require_once(modification(DIR_SYSTEM . 'engine/action.php'));
require_once(modification(DIR_SYSTEM . 'engine/controller.php'));
require_once(modification(DIR_SYSTEM . 'engine/event.php'));
require_once(modification(DIR_SYSTEM . 'engine/front.php'));
require_once(modification(DIR_SYSTEM . 'engine/loader.php'));
require_once(modification(DIR_SYSTEM . 'engine/model.php'));
require_once(modification(DIR_SYSTEM . 'engine/registry.php'));
require_once(modification(DIR_SYSTEM . 'engine/proxy.php'));

// Helper
require_once(DIR_SYSTEM . 'helper/general.php');
require_once(DIR_SYSTEM . 'helper/utf8.php');
require_once(DIR_SYSTEM . 'helper/json.php');
// Registry
$registry = new Registry();

// Config
$config = new Config();
$config->load('default');
$config->load('admin');
$registry->set('config', $config);

// Database
$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
$registry->set('db', $db);

// Settings
$query = $db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '0'");

foreach ($query->rows as $setting) {

	if (!$setting['serialized']) {
		$config->set($setting['key'], $setting['value']);
	} else {
		$config->set($setting['key'], json_decode($setting['value']));
	}
}

// Loader
$loader = new Loader($registry);
$registry->set('load', $loader);

// Url
$url = new Url(HTTP_SERVER, $config->get('config_secure') ? HTTPS_SERVER : HTTP_SERVER);
$registry->set('url', $url);

// Log
$log = new Log($config->get('config_error_filename'));
$registry->set('log', $log);


// Event
$event = new Event($registry);
$registry->set('event', $event);


$languages = array();

$query = $db->query("SELECT * FROM `" . DB_PREFIX . "language`");

$language = new Language($config->get('language_default'));
$language->load($config->get('language_default'));
$registry->set('language', $language);

$language_ok = false;

foreach ($query->rows as $result) {
  if($result['code'] == 'nl-nl'){
    $language = new Language($config->get('language_default'));

    $config->set('config_language_id',$result['language_id']);
    $language->load($config->get('nl-nl'));
    $registry->set('language', $language);
    $language_ok = true;
  }
  $languages[$result['code']] = $result;
}

if(!$language_ok){
  header('HTTP/1.1 500 Internal server error');
  echo('ERROR unable to find nl-nl');
  die;
}

$config->set('languages', $languages);

function error_handler($errno, $errstr, $errfile, $errline) {
	global $log, $config;

	switch ($errno) {
		case E_NOTICE:
		case E_USER_NOTICE:
			$error = 'Notice';
			break;
		case E_WARNING:
		case E_USER_WARNING:
			$error = 'Warning';
			break;
		case E_ERROR:
		case E_USER_ERROR:
			$error = 'Fatal Error';
			break;
		default:
			$error = 'Unknown';
			break;
	}
  // $trace = array_reverse(debug_backtrace());
  //\Developer\Log::debug($trace);
  // $ex = explode("\n", $trace);
	$log->write('PHP ' . $error . ':  ' . $errstr . ' in ' . $errfile . ' on line ' . $errline);
	return true;
}

// Error Handler
set_error_handler('error_handler');

// Cache
$cache = new Cache('file');
$registry->set('cache', $cache);

// Get MY endpoint for authentication against the ACL
$endpoint = \Dge\App::getEndpoint($settings['api_dir']);
// Get AUTH tokens
$xauth = isset($_SERVER['HTTP_X_AUTH_TOKEN']) ? $_SERVER['HTTP_X_AUTH_TOKEN'] : '';
$xservice = isset($_SERVER['HTTP_X_SERVICE_TOKEN']) ? $_SERVER['HTTP_X_SERVICE_TOKEN'] : '';
// Check AUTH tokens
$service_data = \Helper\AclClient::getService($xauth, $xservice, $endpoint,'admin',$settings['acl']);
$registry->set('acl', $service_data);
list($controller, $params, $contenttype) = \Dge\App::getRequestDetails($settings['api_dir']);
// Launch framework
if($controller === 'index'){
	Dge\App::loadController($controller, $params, $contenttype, $registry);
// }else if(true){ // EVD dev hack
}else if($service_data){
	Dge\App::loadController($controller, $params, $contenttype, $registry);
}else{
	header("HTTP/1.1 401 Unauthorized");
	Dge\App::clear();
}
die;
